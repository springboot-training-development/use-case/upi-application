package com.jaga.upiapplication;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.any;


import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.jaga.upiapplication.dto.CustomerRegistrationDTO;
import com.jaga.upiapplication.entity.Customer;
import com.jaga.upiapplication.feignclient.FundTransferClient;
import com.jaga.upiapplication.repo.CustomerRepository;
import com.jaga.upiapplication.service.impl.CustomerRegistrationServiceImpl;

@ExtendWith(MockitoExtension.class)
public class CustomerRegistrationServiceImplTests {

	@InjectMocks
	private CustomerRegistrationServiceImpl customerRegistrationServiceImpl;
	
	@Mock
	private CustomerRepository customerRepository;
	
	@Mock
	private FundTransferClient client;
	
	
	@Test
	void registerCustomerTest() {
		@SuppressWarnings("rawtypes")
		ResponseEntity responseEntity = new ResponseEntity<>("Customer successfully registered in UPI with " , HttpStatus.OK);
		when(client.validateCustomerPhoneNumber(anyString())).thenReturn(responseEntity);
		when(customerRepository.save(any())).thenReturn(new Customer(1L, "jaga", "pinakana", 25, "9963602793"));
		CustomerRegistrationDTO customerRegistrationRequest = buildRegistrationRequest();
		ResponseEntity<?> customerCreatedResponse = customerRegistrationServiceImpl.registerCustomer(customerRegistrationRequest);
		System.out.println(customerCreatedResponse.toString());
		assertEquals(200, customerCreatedResponse.getStatusCodeValue());
	}


	private CustomerRegistrationDTO buildRegistrationRequest() {
		return new CustomerRegistrationDTO("jaga", "pinakana", 25, "9963602793");
	}


	private ResponseEntity<?> buildCustomerValidationSuccessResponse() {
		ResponseEntity<?> response =  new ResponseEntity<>("Phone number existed ",HttpStatus.OK);
		return response;
	}

}
