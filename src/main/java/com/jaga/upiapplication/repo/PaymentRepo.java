package com.jaga.upiapplication.repo;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.jaga.upiapplication.entity.PaymentHistory;

public interface PaymentRepo  extends PagingAndSortingRepository<PaymentHistory, Long>{


	List<PaymentHistory> findByPayerPhoneNumberOrPayeePhoneNumber(String payerAccountNumber, String payeeAccountNumber,
			Pageable pageable);

}
