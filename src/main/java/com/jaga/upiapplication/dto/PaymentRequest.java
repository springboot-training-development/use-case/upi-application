package com.jaga.upiapplication.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class PaymentRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@NotNull(message = "{upi.payment.amount.notnull}")
	private Double payAmount;
	
	@NotNull(message = "{upi.payment.payerphone.notnull}")
	private String payerPhoneNumber;
	
	@NotNull(message = "{upi.payment.payeephone.notnull}")
	private String payeePhoneNumber;
	
}
