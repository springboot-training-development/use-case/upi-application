/**
 * 
 */
package com.jaga.upiapplication.dto;


import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author jaga
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerRegistrationDTO {
	
	@NotNull(message = "{customer.reg.firstname.notnull}")
	private String firstName;
	
	@NotNull(message = "{customer.reg.lastname.notnull}")
	private String lastName;
	
	private int age;
	
	@NotNull(message = "{customer.reg.phoneNumber.notnull}")
	private String phoneNumber;
	
}
