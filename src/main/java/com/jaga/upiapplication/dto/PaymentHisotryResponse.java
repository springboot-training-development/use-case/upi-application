package com.jaga.upiapplication.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class PaymentHisotryResponse implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String payerPhoneNumber;

	private String payeePhoneNumber;

	private Double payAmount;

	private Date paymentDate;
	
	private String remarks;
}
