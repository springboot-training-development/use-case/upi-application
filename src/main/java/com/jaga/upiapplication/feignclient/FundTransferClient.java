package com.jaga.upiapplication.feignclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.jaga.upiapplication.dto.PaymentRequest;

@FeignClient(name = "http://FUND-TRANSFER-SERVICE/bank")
public interface FundTransferClient {

	@GetMapping("/customer/{phoneNumber}/validate")
	public ResponseEntity<?> validateCustomerPhoneNumber(@PathVariable("phoneNumber") String phoneNumber);
	
	@PostMapping("/fund-transfer-by-phonenumber")
	public ResponseEntity<?> processPayment(@RequestBody PaymentRequest payment);
	
}
