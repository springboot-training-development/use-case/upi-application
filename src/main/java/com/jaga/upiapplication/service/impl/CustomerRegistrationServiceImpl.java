package com.jaga.upiapplication.service.impl;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.jaga.upiapplication.dto.CustomerRegistrationDTO;
import com.jaga.upiapplication.entity.Customer;
import com.jaga.upiapplication.feignclient.FundTransferClient;
import com.jaga.upiapplication.repo.CustomerRepository;
import com.jaga.upiapplication.service.CustomerRegistrationService;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class CustomerRegistrationServiceImpl implements CustomerRegistrationService{

	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	FundTransferClient client;
	
	@Override
	public ResponseEntity<?> registerCustomer(final CustomerRegistrationDTO registrationDTO) {
		Optional<Customer> upiCustomer = customerRepository.findByPhoneNumber(registrationDTO.getPhoneNumber());
		if (upiCustomer.isPresent()) {
			return new ResponseEntity<>(
					"Customer already registered in UPI with " + registrationDTO.getPhoneNumber(),
					HttpStatus.BAD_REQUEST);

		} else {
			ResponseEntity<?> customerPhoneValidationReponse = client.validateCustomerPhoneNumber(registrationDTO.getPhoneNumber());
			log.info("customer phone Validation response"+customerPhoneValidationReponse);
			if (customerPhoneValidationReponse.getStatusCodeValue() > 200) {
				return customerPhoneValidationReponse;
			} else {
				Customer customer = new Customer();
				BeanUtils.copyProperties(registrationDTO, customer);
				customerRepository.save(customer);
				return new ResponseEntity<>(
						"Customer successfully registered in UPI with " + registrationDTO.getPhoneNumber(), HttpStatus.OK);
			}
		}
	}

}
