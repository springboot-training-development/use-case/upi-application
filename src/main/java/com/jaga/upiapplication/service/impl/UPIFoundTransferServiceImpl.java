package com.jaga.upiapplication.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.jaga.upiapplication.dto.PaymentHisotryResponse;
import com.jaga.upiapplication.dto.PaymentRequest;
import com.jaga.upiapplication.entity.PaymentHistory;
import com.jaga.upiapplication.feignclient.FundTransferClient;
import com.jaga.upiapplication.repo.PaymentRepo;
import com.jaga.upiapplication.service.UPIFoundTransferService;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class UPIFoundTransferServiceImpl implements UPIFoundTransferService{

	@Autowired
	FundTransferClient client;
	
	@Autowired
	PaymentRepo paymentRepo;
	
	@Override
	public ResponseEntity<?> processPayment(@Valid PaymentRequest paymentRequest) {
		ResponseEntity<?> paymentResponse = client.processPayment(paymentRequest);
		log.info("Upi paymnet by phone number payment Response"+paymentResponse);
		if (paymentResponse.getStatusCodeValue() > 200) {
			return paymentResponse;
		} else {
			PaymentHistory paymenthistory = new PaymentHistory();
			BeanUtils.copyProperties(paymentRequest, paymenthistory);
			paymenthistory.setPaymentDate(new Date());
			paymentRepo.save(paymenthistory);
			return paymentResponse;
		}
		
	}

	@Override
	public ResponseEntity<?> getPaymentHistoryByPhoneNumber(String phoneNumber, int pageNo, int noOfRecords) {
		Pageable sortedByDateDesc = PageRequest.of(pageNo, noOfRecords, Sort.by("paymentDate").descending());
		List<PaymentHistory> paymentHistoryList = paymentRepo.findByPayerPhoneNumberOrPayeePhoneNumber(phoneNumber, phoneNumber, sortedByDateDesc);
		List<PaymentHisotryResponse> paymentHisotryResponses = new ArrayList<>();
		for (PaymentHistory paymentHistory : paymentHistoryList) {
			PaymentHisotryResponse paymentHisotryResponse =  new PaymentHisotryResponse();
			BeanUtils.copyProperties(paymentHistory, paymentHisotryResponse);
			if (paymentHistory.getPayerPhoneNumber().equals(phoneNumber)) {
				paymentHisotryResponse.setRemarks("debit");
			} else {
				paymentHisotryResponse.setRemarks("credit");
			}
			paymentHisotryResponses.add(paymentHisotryResponse);
		}
		return new ResponseEntity<>(paymentHisotryResponses, HttpStatus.OK);
	}
	
}
