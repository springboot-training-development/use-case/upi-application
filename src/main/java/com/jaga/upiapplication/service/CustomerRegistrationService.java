package com.jaga.upiapplication.service;

import org.springframework.http.ResponseEntity;

import com.jaga.upiapplication.dto.CustomerRegistrationDTO;

public interface CustomerRegistrationService {

	ResponseEntity<?> registerCustomer(final CustomerRegistrationDTO registrationDTO);

}
