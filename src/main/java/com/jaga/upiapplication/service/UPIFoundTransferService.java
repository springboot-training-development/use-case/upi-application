package com.jaga.upiapplication.service;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;

import com.jaga.upiapplication.dto.PaymentRequest;

public interface UPIFoundTransferService {

	ResponseEntity<?> processPayment(@Valid PaymentRequest paymentRequest);

	ResponseEntity<?> getPaymentHistoryByPhoneNumber(final String phoneNumber, int pageNo, int noOfRecords);

}
