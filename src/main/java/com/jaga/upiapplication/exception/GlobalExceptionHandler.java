package com.jaga.upiapplication.exception;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.jaga.upiapplication.dto.ErrorDetails;

import feign.FeignException;
import lombok.extern.log4j.Log4j2;

@ControllerAdvice
@Log4j2
public class GlobalExceptionHandler {

	@Autowired
	MessageSource messageSource;

	@ExceptionHandler
	protected ResponseEntity<?> handle(HttpMessageNotReadableException messageNotReadableException) {
		return new ResponseEntity<>(messageSource.getMessage("invalid.format", null, Locale.ENGLISH), HttpStatus.BAD_REQUEST);
	}

	
	
	@ExceptionHandler
	protected ResponseEntity<?> handle(MissingServletRequestParameterException missingServletRequestParameterException) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(),
				messageSource.getMessage("validation.fail", null, Locale.ENGLISH),
				Arrays.asList(missingServletRequestParameterException.getParameterName()));
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}
	@ExceptionHandler
	protected ResponseEntity<?> handle(MethodArgumentNotValidException messageNotReadableException) {
		BindingResult result = messageNotReadableException.getBindingResult();
		List<String> errors = new ArrayList<>();
		result.getAllErrors().stream().forEach(error -> {
			errors.add(error.getDefaultMessage());
			log.error("error details " + error.getDefaultMessage());
		});
		ErrorDetails errorDetails = new ErrorDetails(new Date(), messageSource.getMessage("validation.fail", null, Locale.ENGLISH), errors);
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler
	protected ResponseEntity<?> handle(MethodArgumentTypeMismatchException messageNotReadableException) {
		return new ResponseEntity<>(messageSource.getMessage("invalid.format", null, Locale.ENGLISH), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler
	protected ResponseEntity<?> handle(FeignException feignException) {
		log.error("feignException for request"+feignException.request());
		log.error("response"+feignException.responseBody());
		if (feignException.content() != null) {
			return new ResponseEntity<>(new String(feignException.content()), HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}