package com.jaga.upiapplication.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jaga.upiapplication.dto.PaymentRequest;
import com.jaga.upiapplication.service.UPIFoundTransferService;

import lombok.extern.log4j.Log4j2;

/**
 * @author jaga
 *
 */
@RestController
@RequestMapping("/upi")
@Log4j2
public class UPIFundtransferController {

	@Autowired
	UPIFoundTransferService foundTransferService;

	@PostMapping("/fund-transfer-by-phonenumber")
	public ResponseEntity<?> processPaymentByUpiPhoneNumber(@Valid @RequestBody PaymentRequest paymentRequest) {
		log.info("request for Payment By Upi PhoneNumber  "+paymentRequest);
		return  foundTransferService.processPayment(paymentRequest);
	}
	
}
