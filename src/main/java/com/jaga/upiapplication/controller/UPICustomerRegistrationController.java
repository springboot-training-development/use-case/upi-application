/**
 * 
 */
package com.jaga.upiapplication.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jaga.upiapplication.dto.CustomerRegistrationDTO;
import com.jaga.upiapplication.service.CustomerRegistrationService;

import lombok.extern.log4j.Log4j2;

/**
 * @author jaga
 *
 */
@RestController
@RequestMapping("/upi")
@Log4j2
public class UPICustomerRegistrationController {

	@Autowired
	CustomerRegistrationService customerRegistrationService;

	@GetMapping("test")
	public ResponseEntity<?> testApi() {
		return new ResponseEntity<>("I am running...", HttpStatus.OK);
	}
	
	@PostMapping("/registercustomer")
	public ResponseEntity<?> registerCustomer(@Valid @RequestBody CustomerRegistrationDTO registrationDTO) {
		log.info("request for customer registration "+registrationDTO);
		return  customerRegistrationService.registerCustomer(registrationDTO);
	}
}
