/**
 * 
 */
package com.jaga.upiapplication.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jaga.upiapplication.service.UPIFoundTransferService;

import lombok.extern.log4j.Log4j2;

/**
 * @author jaga
 *
 */
@RestController
@RequestMapping("/upi")
@Log4j2
public class UPIStatementController {

	@Autowired UPIFoundTransferService foundTransferService;
	
	@GetMapping("/payment-history-by-phonenumber")
	public ResponseEntity<?> getPaymentHistory(@RequestParam @Valid String phoneNumber, @RequestParam int pageNo,
			@RequestParam int noOfRecords) {
		log.info(" payment history for request "+phoneNumber);
		return foundTransferService.getPaymentHistoryByPhoneNumber(phoneNumber, pageNo, noOfRecords);
	}
}
